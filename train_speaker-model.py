import numpy as np
import os
from os.path import exists
import librosa
import tensorflow as tf
import keras
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras import backend as K
from keras.models import Model
from keras.layers import Dense, Conv2D, Input, Flatten, Lambda
from capsulelayers import CapsuleLayer, PrimaryCap
from keras.utils import multi_gpu_model,to_categorical
from keras import callbacks
from argparse import ArgumentParser

# python train_speaker-model.py --data-dir datasets_preprocess/vctk/ --save-dir siamese_model/

params = {
    "BATCH_SIZE": 1,
    "EPOCHS"    : 45,
    "ROUTINGS"  : 2,
    "N_MELS"    : 40
}

# pesan ini di generate oleh anak sg : " HALO KAK ISAAAAAALLLLLLLLLLLLLLL "
# hallo

def arg_parse():
    arg_p = ArgumentParser()
    arg_p.add_argument('--data-dir', required=True)
    arg_p.add_argument('--save-dir', required=True)
    arg_p.add_argument('--num-gpu', default=1, type=int)
    return arg_p.parse_args()

def embedModel(input):
    """
    :param num_class:
    :return:
    """
    x = input
    num_label = 5
    conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1')(x)
    primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid', name='primarycap_conv2d')
    classcaps = CapsuleLayer(num_capsule=num_label, dim_capsule=8, routings=params['ROUTINGS'],
                             name='classcaps')(primarycaps)

    out_caps = Flatten(name='capsnet')(classcaps)
    fc6 = Dense(2048, activation='relu', input_dim=16 * num_label)(out_caps)
    fc7 = Dense(1024, activation='relu')(fc6)
    out = Dense(num_label, activation='softmax')(fc7)
    model = Model(inputs=x, outputs=out)

    model.load_weights('embedding_model/embedding_last_epoch.hdf5')
    for layer in model.layers[:]:
        layer.trainable = False

    out = model.layers[-2].output

    return out

def speakerModel(input):
    conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1_l')(input)
    primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid',
                             name="primarycap_conv2d_l")
    classcaps = CapsuleLayer(num_capsule=2, dim_capsule=8, routings=params['ROUTINGS'],
                             name='classcaps_l')(primarycaps)

    out_caps = Flatten(name='capsnet_l')(classcaps)
    fc6 = Dense(2048, activation='relu', input_dim=16 * 2)(out_caps)
    out= Dense(1024, activation='relu')(fc6)

    return out


def siameseModel():
    """
    :return: TensorFlow model
    """
    x_l = Input(shape=(40, 188, 1))
    x_r = Input(shape=(40, 188, 1))

    embedOut = embedModel(input=x_r)
    speakerOut = speakerModel(input=x_l)

    L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([speakerOut, embedOut])
    prediction = Dense(1, activation='sigmoid')(L1_distance)

    siamese_net = Model(inputs=[x_l, x_r], outputs=prediction)
    siamese_net.compile(loss='binary_crossentropy',
                        optimizer=keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0),
                        metrics=['accuracy'])

    siamese_net.summary()
    return siamese_net

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    num_class = len(labels)
    return labels, label_indices, num_class

def callback(args):
    """
    :return: [log, tb, checkpoint]
    """
    log = callbacks.CSVLogger(args.save_dir + '/log.csv')
    tb = callbacks.TensorBoard(log_dir=args.save_dir + '/tensorboard-logs',
                               batch_size=args.save_dir, histogram_freq=0, write_graph=True, write_images=True)
    filepath = args.save_dir + "weights-improvement2-{epoch:02d}-{acc:.2f}.hdf5"
    checkpoint = callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=0,
                                           save_best_only=True, mode='max')
    return [log, tb, checkpoint]

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    num_class = len(labels)
    return labels, label_indices, num_class

def get_train(args, split_ratio=0.8, random_state=None):
    """
    Get available labels
    :param label:
    :param split_ratio:If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the test split.
    If int, represents the absolute number of test samples. If None, the value is set to the complement of the train size. By default, the value is set to 0.25. The default will change in version 0.21.
    It will remain 0.25 only if train_size is unspecified, otherwise it will complement the specified train_size.
    :param random_state: If int, random_state is the seed used by the random number generator; If RandomState instance,
    random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

    :return:
    """
    labels, indices, _ = get_labels("datasets/VCTK-Corpus/wav48")
    # Getting first arrays
    X = np.load(args.data_dir + labels[0] + '.npy')
    X_true = X

    # Append all of the dataset into one single array, same goes for y
    for i, label in enumerate(labels[1:]):
        x = np.load(args.data_dir + label + '.npy')
        X = np.vstack((X, x))

    N, mels, f = X.shape
    qwe = X_true.shape[0] * N
    print(qwe)
    pairs = [np.zeros((qwe, mels, f)) for _ in range(2)]
    target = np.zeros(pairs[0].shape[0])

    idx=0
    for i in range(len(X_true)):
        for j in range(len(X)):
            pairs[0][idx] = X_true[i]
            pairs[1][idx] = X[j]
            # print(sample1.shape == sample2.shape)

            if np.all(pairs[0][idx] == pairs[1][idx]):
                target[idx] = 1
            else:
                target[idx] = 0
            idx += 1

    N = len(target)
    assert pairs[0].shape[0] == len(target)
    # X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=(1 - split_ratio), random_state=random_state,
    #                                                     shuffle=True)
    pairs[0] = pairs[0].reshape((N, mels,f, 1))
    pairs[1] = pairs[1].reshape((N, mels,f, 1))

    return pairs, target

if __name__ == '__main__':
    args = arg_parse()
    labels, indices, num_class = get_labels("datasets/VCTK-Corpus/wav48")
    # print(labels)

    with tf.device('/gpu:0'):
        model  = siameseModel()

    """
        2. Checkpoint
        Define callbacks_list for training, it's containing Log, TensorBoard, and Checkpoint
        I currently NOT using it, but if you want to try it you can add inside model.fit() method
        as 'callback=callback_list'
    """
    callbacks_list = callback(args)

    """
        3. Let's rockin' it!
    """
    X_train, y_train = get_train(args)

    if args.num_gpu > 1:
        # define muti-gpu model
        multi_model = multi_gpu_model(model, gpus=args.num_gpu)
        multi_model.fit(X_train, y_train, batch_size=params['BATCH_SIZE'], epochs=params['EPOCHS'], verbose=1,
                        callbacks=callbacks_list)
    else:
        model.fit(X_train, y_train, batch_size=params['BATCH_SIZE'], epochs=params['EPOCHS'], verbose=1)

    if not exists(args.save_dir):
        os.makedirs(args.save_dir)

    model.save(args.save_dir + 'siameseModel_last_epoch.hdf5')


