Loving can hurt, loving can hurt sometimes
But it's the only thing that I know
When it gets hard, you know it can get hard sometimes
It is the only thing makes us feel alive
We keep this love in a photograph
We made these memories for ourselves
Where our eyes are never closing
Hearts are never broken
And time's forever frozen still
So you can keep me
Inside the pocket of your ripped jeans
Holding me closer until our eyes meet
You won't ever be alone, wait for me to come home
Loving can heal, loving can mend your soul
And it's the only thing that I know, know
I swear it will get easier
Remember that with every piece of you
and it's the only thing we take with us when we die
we keep this love in this photograph
We made these memories for ourselves
Where our eyes are never closing
Hearts were never broken
And time's forever frozen still
So you can keep me
Inside the pocket of your ripped jeans
Holding me closer 'til our eyes meet
You won't ever be alone
And if you hurt me
That's okay baby, only words bleed
Inside these pages you just hold me
And I won't ever let you go
Wait for me to come home
Wait for me to come home
Wait for me to come home
Wait for me to come home
Oh, you can fit me
Inside the necklace you got when you were sixteen
Next to your heartbeat where I should be
Keep it deep within your soul
And if you hurt me
Well, that's okay baby, only words bleed
Inside these pages you just hold me
And I won't ever let you go
When I'm away, I will remember how you kissed me
Under the lamppost back on Sixth street
Hearing you whisper through the phone
Wait for me to come home
Met a girl at seventeen
Thought she meant the world to me
So I gave her everything
She turned out to be a cheat
Said she'd been thinking for a long time
And she found somebody new
I've been thinking that this whole time
Well I never thought you'd stay
That's okay
I hope he takes your filthy heart
And then he throws you away someday
Before you go, there's one thing you oughta know
I don't wanna take your precious time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
I don't wanna take up all your time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
You're the lowest type
You're the lowest
I met a girl talking away
She found a boy she knew she'd change
I changed my clothes, my hair, my face
To watch us go our separate ways
She said we've grown apart for sometime
But then she found somebody new
I hope mister Right puts up with all the bullshit that you do
Stay the hell away,
While I sit here by myself
And figure out how I got this way
Before you go, there's one thing you oughta know
I don't wanna take your precious time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
I don't wanna take up all your time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
I don't want to get things confused
She said she'd never settle for some boy she couldn't use
So now I gotta call the doctor
So he can prescribe me medication
So I can deal with all the memories of being here this way
I met a girl at twenty-three
Knew she meant the world to me
So I gave her everything
And she did the same for me
Imagine that
'Cause you're such a pretty, pretty face
No you're such a pretty, pretty face
Well, oh yeah
'Cause you're such a pretty, pretty face
No you're such a pretty, pretty face
I don't wanna take your precious time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
I don't wanna take up all your time
'Cause you're such a pretty, pretty, pretty, pretty face
But you turned into a pretty big waste of my time
