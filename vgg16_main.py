import numpy as np
import os
import keras
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from keras import optimizers
from keras.models import Sequential, Model
from keras import backend as K
from keras.layers import Dense, Conv2D, MaxPooling2D,Convolution2D, Dropout, Input, Flatten, Subtract, merge,ZeroPadding2D
from capsulelayers import CapsuleLayer, PrimaryCap, Length, Mask
from keras.utils import multi_gpu_model
from keras import callbacks
import tensorflow as tf

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
DATA_PATH = "wav/"
DATA_SAVED = "wav_onevsall/"
MODEL_SAVED = "model_vgg16/"
loadModel = False
TESTING = False
modelName = "routing4-bz100/weights-improvement-07-0.87"
num_gpu = 1

BATCH_SIZE = 50
EPOCHS = 3
ROUTINGS = 1

# Input: Folder Path
# Output: Tuple (Label, Indices of the labels, one-hot encoded labels)
def get_labels(path=DATA_PATH):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices, to_categorical(label_indices)

def get_train_test(label, split_ratio=0.8, random_state=42):
    # Get available labels
    labels, indices, _ = get_labels(DATA_PATH)

    # Getting first arrays
    X = np.load(DATA_SAVED + label + '.npy')
    Y = np.load(DATA_SAVED + "classes-" + label + '.npy')
    num_class = 2

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=(1 - split_ratio), random_state=random_state, shuffle=True)

    return X_train, X_test, y_train, y_test, num_class

def buildModel(num_class):
    model = Sequential([
        Conv2D(64, (3, 3), input_shape=(63, 63, 1), padding='same', activation ='relu'),
        Conv2D(64, (3, 3), activation='relu', padding ='same'),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        Conv2D(128, (3, 3), activation='relu', padding ='same'),
        Conv2D(128, (3, 3), activation='relu', padding ='same', ),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        Conv2D(256, (3, 3), activation='relu', padding ='same', ),
        Conv2D(256, (3, 3), activation='relu', padding ='same', ),
        Conv2D(256, (3, 3), activation='relu', padding ='same', ),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # Conv2D(512, (3, 3), activation='relu', padding ='same', ),
        # MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        Flatten(),
        Dense(4096, activation='relu'),
        Dense(4096, activation='relu'),
        Dense(num_class, activation='softmax')
    ])
    model.compile(loss='MSE',
                  optimizer=optimizers.SGD(lr=0.00001, decay=1e-6, momentum=0.9, nesterov=True),
                  metrics=['accuracy'])

    model.summary()

    return model

def reshapeData(X_train, X_test, y_train, y_test):
    X_train = X_train.reshape(X_train.shape[0], 63, 63, 1)
    X_test = X_test.reshape(X_test.shape[0], 63, 63, 1)
    y_train_hot = to_categorical(y_train)
    y_test_hot = to_categorical(y_test)

    return X_train, X_test, y_train_hot, y_test_hot

def test(model, data):
    x_test, y_test = data
    y_pred, x_recon = model.predict(x_test, batch_size=100)
    print('-'*30 + 'Begin: test' + '-'*30)
    print('Test acc:', np.sum(np.argmax(y_pred, 1) == np.argmax(y_test, 1))/y_test.shape[0])

if __name__=="__main__":
    labels, _, _ = get_labels(DATA_PATH)

    # for label in labels:
    label = 'id10001'
    X_train, X_test, y_train, y_test, num_class = get_train_test(label=label)
    X_train, X_test, y_train_hot, y_test_hot = reshapeData(X_train, X_test, y_train, y_test)
	
    with tf.device('/gpu:0'):
        model = buildModel(num_class)

    # build model
    if loadModel:
        model.load_weights(MODEL_SAVED + modelName + ".hd5")
        # model.load_weights('routing4-bz100/weights-improvement-07-0.87.hdf5')
        # history = np.load(modelName + "-history.npy").item()
    else:
        None

    # checkpoint
    log = callbacks.CSVLogger(MODEL_SAVED + '/log.csv')
    tb = callbacks.TensorBoard(log_dir=MODEL_SAVED + '/tensorboard-logs',batch_size=BATCH_SIZE)
    filepath = MODEL_SAVED + "weights-improvement-{epoch:02d}-{val_acc:.2f}.hdf5"
    checkpoint = callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=0, save_best_only=True, mode='max')
    callbacks_list = [log,tb,checkpoint]

    # configure number of gpu
    if not TESTING:
        if num_gpu > 1:
            # define muti-gpu model
            multi_model = multi_gpu_model(model, gpus=num_gpu)
            multi_model.fit(X_train, y_train_hot, batch_size=BATCH_SIZE, epochs=EPOCHS, verbose=1,
                            validation_data=(X_test, y_test_hot))
        else:
            model.fit(X_train, y_train_hot, batch_size=BATCH_SIZE, epochs=EPOCHS, verbose=1,
                            validation_data=(X_test, y_test_hot))

        model.save(MODEL_SAVED+ label+'.hd5')
    else:
        test(model=model,data=(X_test,y_test))


