import os
import keras
import librosa
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from os.path import exists
from keras import callbacks
from keras.models import Model
from argparse import ArgumentParser
from sklearn.preprocessing import LabelEncoder
from capsulelayers import CapsuleLayer, PrimaryCap
from keras.utils import multi_gpu_model,to_categorical
from keras.layers import Dense, Conv2D, Input, Flatten

# python train_embed.py --mode train --data-dir datasets_preprocess/vctk/ --save-dir embedding_model/
# python train_embed.py --mode evaluate --data-dir datasets_preprocess/vctk/ --save-dir embedding_model/ --checkpoint embedding_model/embedding_last_epoch.hdf5

params = {
    "BATCH_SIZE": 8,
    "EPOCHS"    : 10,
    "ROUTINGS"  : 2,
    "N_MELS"    : 40
}

def arg_parse():
    arg_p = ArgumentParser()
    arg_p.add_argument('--mode', required=True)
    arg_p.add_argument('--data-dir', required=True)
    arg_p.add_argument('--save-dir', required=True)
    arg_p.add_argument('--checkpoint')
    arg_p.add_argument('--num-gpu', default=1, type=int)
    return arg_p.parse_args()

def embedModel(num_class):
    """
    :param num_class:
    :return:
    """
    x = Input(shape=(40, 188, 1))
    conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1')(x)
    primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid', name="primarycaps_conv2d")
    classcaps = CapsuleLayer(num_capsule=num_class, dim_capsule=8, routings=params['ROUTINGS'],
                             name='classcaps')(primarycaps)

    out_caps = Flatten(name='capsnet')(classcaps)
    fc6 = Dense(2048, activation='relu', input_dim=16 * num_class)(out_caps)
    fc7 = Dense(1024, activation='relu')(fc6)
    out = Dense(num_class, activation='softmax')(fc7)
    model = Model(inputs=x, outputs=out)

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0),
                  metrics=['accuracy'])

    model.summary()
    return model

def callback(args):
    """
    :return: [log, tb, checkpoint]
    """
    log = callbacks.CSVLogger(args.save_dir + '/log.csv')
    tb = callbacks.TensorBoard(log_dir=args.save_dir + '/tensorboard-logs',
                               batch_size=args.save_dir,histogram_freq=0,write_graph=True, write_images=True)
    filepath = args.save_dir + "weights-improvement2-{epoch:02d}-{acc:.2f}.hdf5"
    checkpoint = callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=0,
                                           save_best_only=True, mode='max')
    return [log, tb, checkpoint]

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    num_class = len(labels)
    return labels, label_indices, num_class

def get_train_test(args, split_ratio=0.8, random_state=None):
    """
    Get available labels
    :param label:
    :param split_ratio:If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the test split.
    If int, represents the absolute number of test samples. If None, the value is set to the complement of the train size. By default, the value is set to 0.25. The default will change in version 0.21.
    It will remain 0.25 only if train_size is unspecified, otherwise it will complement the specified train_size.
    :param random_state: If int, random_state is the seed used by the random number generator; If RandomState instance,
    random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

    :return:
    """
    labels, indices, _ = get_labels("datasets/VCTK-Corpus/wav48")
    # Getting first arrays
    X = np.load(args.data_dir + labels[0] + '.npy')
    Y = np.load(args.data_dir + "classes-" + labels[0] + '.npy')

    # Append all of the dataset into one single array, same goes for y
    for i, label in enumerate(labels[1:]):
        x = np.load(args.data_dir + label + '.npy')
        y = np.load(args.data_dir + "classes-" + label + '.npy')
        X = np.vstack((X, x))
        Y = np.concatenate((Y, y))

    encoder = LabelEncoder()
    Y = encoder.fit_transform(Y)

    assert X.shape[0] == len(Y)
    # X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=(1 - split_ratio), random_state=random_state,
    #                                                     shuffle=True)
    return X, Y

def reshapeData(X_train, y_train):
    """
    :param X_train:
    :param X_test:
    :param y_train:
    :param y_test:
    :return:
    """
    X_train = X_train.reshape(X_train.shape[0], params["N_MELS"], 188, 1)
    y_train_hot = to_categorical(y_train)

    return X_train, y_train_hot

def wav2mfcc(wave):
    """
    :param wave:
    :return:
    """
    max_pad_len = 188
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=params['N_MELS'])
    pad_width = max_pad_len - mfcc.shape[1]
    mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def get_dataeval(args, label):
    """
    Get available labels
    :param label:
    :param split_ratio:If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the test split.
    If int, represents the absolute number of test samples. If None, the value is set to the complement of the train size. By default, the value is set to 0.25. The default will change in version 0.21.
    It will remain 0.25 only if train_size is unspecified, otherwise it will complement the specified train_size.
    :param random_state: If int, random_state is the seed used by the random number generator; If RandomState instance,
    random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

    :return:
    """
    labels, indices, _ = get_labels("datasets/VCTK-Corpus/wav48")

    # Getting first arrays
    X = np.load(args.data_dir + label + '.npy')
    Y = np.load(args.data_dir + "classes-" + label + '.npy')

    assert X.shape[0] == len(Y)

    return X, Y

def evaluate(args, labels, num_class):
    savedir = args.save_dir + 'dvectors/'

    if not exists(savedir):
        os.makedirs(savedir)

    with tf.device('/gpu:0'):
        model = embedModel(num_class)
        model.load_weights(args.checkpoint)
        model.layers.pop(0)

    for label in tqdm(labels):

        x_train, classes = get_dataeval(args,label)

        x_train = np.array(x_train)
        x_train = x_train.reshape(x_train.shape[0], 40, 188, 1)

        d_vectors = model.predict(x_train)

        np.save(savedir + label + '.npy', d_vectors)
        np.save(savedir + "classes-" + label + '.npy', classes)

def train(args, num_class):
    """
        1. Build Model
        The Capsule Network model.
    """
    with tf.device('/gpu:0'):
        model = embedModel(num_class)

    """
        2. Checkpoint    
        Define callbacks_list for training, it's containing Log, TensorBoard, and Checkpoint
        I currently NOT using it, but if you want to try it you can add inside model.fit() method
        as 'callback=callback_list'
    """
    callbacks_list = callback(args)

    """
        3. Let's rockin' it!
    """
    X_train, y_train = get_train_test(args)
    X_train, y_train_hot = reshapeData(X_train, y_train)

    if args.num_gpu > 1:
        # define muti-gpu model
        multi_model = multi_gpu_model(model, gpus=args.num_gpu)
        multi_model.fit(X_train, y_train_hot, batch_size=params['BATCH_SIZE'], epochs=params['EPOCHS'], verbose=1,
                        callbacks=callbacks_list)
    else:
        model.fit(X_train, y_train_hot, batch_size=params['BATCH_SIZE'], epochs=params['EPOCHS'], verbose=1,
                  callbacks=callbacks_list)

    if not exists(args.save_dir):
        os.makedirs(args.save_dir)

    model.save(args.save_dir + 'embedding_last_epoch.hdf5')

if __name__=='__main__':
    args = arg_parse()
    labels, indices, num_class = get_labels("datasets/VCTK-Corpus/wav48")

    if args.mode == 'train':
        train(args, num_class)
    else:
        evaluate(args, labels, num_class)


