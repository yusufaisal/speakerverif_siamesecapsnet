import librosa
import numpy as np
import os
from tqdm import tqdm
from argparse import ArgumentParser
from os.path import exists


def arg_parse():
    arg_p = ArgumentParser()
    arg_p.add_argument('--wav-dir', required=True,)
    arg_p.add_argument('--save-dir', required=True)
    return arg_p.parse_args()

def wav2mfcc(wave):
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=40)
    pad_width = 188 - mfcc.shape[1]
    mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices

if __name__ == '__main__':
      args = arg_parse()
      labels, _ = get_labels(args.wav_dir)

      if not exists(args.save_dir):
          os.makedirs(args.save_dir)

      for label in labels:
          # Init mfcc vectors
          mfcc_vectors = []
          classes = []
          for root, subdirs, files in os.walk(os.path.join(args.wav_dir, label)):
              for file in tqdm(files, unit="files"):
                  filepath = os.path.join(root, file)

                  wave, sr = librosa.load(filepath, mono=True, sr=None, offset=0, duration=2)
                  oriWave = wav2mfcc(wave)
                  mfcc_vectors.append(np.flip(oriWave, axis=0))
                  classes.append(label)

          np.save(args.save_dir + label + '.npy', mfcc_vectors)
          np.save(args.save_dir + "classes-" + label + '.npy', classes)