import librosa
import numpy as np
from keras.utils import to_categorical
import os

# DATA_PATH = "wav/"
# DATA_SAVED = "wav_onevsall/"
DATA_PATH = "vctk_datasets/"
DATA_SAVED = "vctk_onevsall/"

def wav2mfcc(wave):
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=40)
    # pad_width = max_pad_len - mfcc.shape[1]
    # mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def get_labels(path=DATA_PATH):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices, to_categorical(label_indices)

def save_data_to_array(path=DATA_PATH, max_pad_len=11):
    labels, _, _ = get_labels(path)

    for target in labels:
        mfcc_vectors = []
        classes = []
        print(target)
        for label in labels:
            # Init mfcc vectors
            for root, subdirs, files in os.walk(os.path.join(path, label)):
                for file in files:
                    filepath = os.path.join(root, file)

                    wave, sr = librosa.load(filepath, mono=True, sr=None, offset=1, duration=2)
                    oriWave = wav2mfcc(wave)
                    if oriWave.shape == (40, 188):
                        mfcc_vectors.append(np.flip(oriWave,axis=0))
                        if target==label:
                            classes.append(1)
                        else:
                            classes.append(0)
        np.save(DATA_SAVED + target + '.npy', mfcc_vectors)
        np.save(DATA_SAVED + "classes-" + target + '.npy', classes)


def augPitch(samples, sample_rate):
    y_pitch = samples.copy()
    bins_per_octave = 24
    pitch_pm = 4
    pitch_change = pitch_pm * 4 * (np.random.uniform() - 0.5)
    # print("pitch_change = ", pitch_change)
    y_pitch = librosa.effects.pitch_shift(y_pitch.astype('float64'),
                                          sample_rate, n_steps=pitch_change,
                                          bins_per_octave=bins_per_octave)
    return y_pitch

def augPitchNSpeed(samples,sample_rate):
    y_pitch_speed = samples.copy()
    # you can change low and high here
    length_change = np.random.uniform(low=0.5, high=1.5)
    speed_fac = 1.0 / length_change
    # print("resample length_change = ", length_change)

    tmp = np.interp(np.arange(0, len(y_pitch_speed), speed_fac), np.arange(0, len(y_pitch_speed)), y_pitch_speed)
    minlen = min(y_pitch_speed.shape[0], tmp.shape[0])
    y_pitch_speed *= 0
    y_pitch_speed[0:minlen] = tmp[0:minlen]

    return y_pitch_speed

def augNoise(samples, sample_rate):
    y_noise = samples.copy()
    # you can take any distribution from https://docs.scipy.org/doc/numpy-1.13.0/reference/routines.random.html
    noise_amp = 0.005 * np.random.uniform() * np.amax(y_noise)
    y_noise = y_noise.astype('float64') + noise_amp * np.random.normal(size=y_noise.shape[0])

    return y_noise

if __name__=='__main__':
    save_data_to_array()