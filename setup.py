from os.path import exists
import os
import subprocess

if not exists('clone'):
    os.makedirs('clone')

if not exists('datasets'):
    os.makedirs('datasets')

if not exists('datasets_preprocess'):
    os.makedirs('datasets_preprocess')

if not exists('model_capsNet'):
    os.makedirs('model_capsNet')

if not exists('test_file'):
    os.makedirs('test_file')

subprocess.call(['conda','install','--file','requirements.txt'])