import librosa
import numpy as np
from keras.utils import to_categorical
import os
from os.path import exists

label ="idx0"
DATA_PATH = "datasets/" + label + "/"
DATA_SAVED = "datasets_preprocess/" + label + "/"
max_pad_len = 188

def wav2mfcc(wave):
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=40)
    pad_width = max_pad_len - mfcc.shape[1]
    mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def get_labels(path=DATA_PATH):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices, to_categorical(label_indices)

def save_data_to_array(path=DATA_PATH, max_pad_len=11):
    labels, _, _ = get_labels(path)

    # for label in labels:
    #     # Init mfcc vectors
    #     mfcc_vectors = []
    #
    #     wavfiles = [path + label + '/' + wavfile for wavfile in os.listdir(path + '/' + label)]
    #     for wavfile in wavfiles:
    #         mfcc = wav2mfcc(wavfile, max_pad_len=max_pad_len)
    #         mfcc_vectors.append(mfcc)
    #     np.save(label + '.npy', mfcc_vectors)

    # for label in labels:
    # Init mfcc vectors

    mfcc_vectors = []
    classes = []
    positif = 0
    negatif = 0
    print(label)
    # for root, subdirs, files in os.walk(os.path.join(path, label)):
    for root, subdirs, files in os.walk(path):
        for file in files:
            filepath = os.path.join(root, file)

            # print(filepath)
            # for i in range(0, 2, 1):

            wave, sr = librosa.load(filepath, mono=True, sr=None, offset=0, duration=2)
            oriWave = wav2mfcc(wave)

            if root==DATA_PATH + "actual_voices":
                mfcc_vectors.append(np.flip(oriWave, axis=0))
                classes.append(1)
                positif += 1
            elif root==DATA_PATH + "test":
                None
            else:
                mfcc_vectors.append(np.flip(oriWave, axis=0))
                classes.append(0)
                negatif +=1

                for i in range(2):
                    aug3 = augPitch(wave, sr)
                    aug3 = wav2mfcc(aug3)
                    mfcc_vectors.append(np.flip(aug3, axis=0))
                    classes.append(0)

                    aug1 = augNoise(wave, sr)
                    aug1 = wav2mfcc(aug1)
                    mfcc_vectors.append(np.flip(aug1, axis=0))
                    classes.append(0)
                    negatif += 2

            # print(oriWave.shape)

            # aug1 = augNoise(wave,sr)
            # aug1 = wav2mfcc(aug1)
            # mfcc_vectors.append(np.flip(aug1,axis=0))
            # classes.append(0)
            # # print(aug1.shape)
            #
            # # aug2 = augPitchNSpeed(wave,sr)
            # # aug2 = wav2mfcc(aug2)
            # # mfcc_vectors.append(aug2)
            # # classes.append(1)
            # # print(aug2.shape)
            #
            # aug3 = augPitch(wave,sr)
            # aug3 = wav2mfcc(aug3)
            # mfcc_vectors.append(np.flip(aug3,axis=0))
            # classes.append(0)
            # # print(aug3.shape)

    if not exists(DATA_SAVED):
        os.makedirs(DATA_SAVED)
    np.save(DATA_SAVED + label + '.npy', mfcc_vectors)
    np.save(DATA_SAVED + "classes-" + label + '.npy', classes)

    print(positif, negatif)


def augPitch(samples, sample_rate):
    y_pitch = samples.copy()
    bins_per_octave = 24
    pitch_pm = np.random.uniform(low=1,high=6)
    pitch_change = pitch_pm * 4 * (np.random.uniform() - 0.5)
    # print("pitch_change = ", pitch_change)
    y_pitch = librosa.effects.pitch_shift(y_pitch.astype('float64'),
                                          sample_rate, n_steps=pitch_change,
                                          bins_per_octave=bins_per_octave)
    return y_pitch

def augPitchNSpeed(samples,sample_rate):
    y_pitch_speed = samples.copy()
    # you can change low and high here
    length_change = np.random.uniform(low=0.5, high=1.5)
    speed_fac = 1.0 / length_change
    # print("resample length_change = ", length_change)

    tmp = np.interp(np.arange(0, len(y_pitch_speed), speed_fac), np.arange(0, len(y_pitch_speed)), y_pitch_speed)
    minlen = min(y_pitch_speed.shape[0], tmp.shape[0])
    y_pitch_speed *= 0
    y_pitch_speed[0:minlen] = tmp[0:minlen]

    return y_pitch_speed

def augNoise(samples, sample_rate):
    y_noise = samples.copy()
    # you can take any distribution from https://docs.scipy.org/doc/numpy-1.13.0/reference/routines.random.html
    noise_amp = 0.005 * np.random.uniform() * np.amax(y_noise)
    y_noise = y_noise.astype('float64') + noise_amp * np.random.normal(size=y_noise.shape[0])

    return y_noise

if __name__=='__main__':
    save_data_to_array()
