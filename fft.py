from scipy.fftpack import fft
import numpy as np
from pprint import pprint

def fft_manual(x):
    for j in range(len(x)):
        x[j] = x[j] * np.exp(-np.sqrt(-1+0j) * 1 * 0 * 2*np.pi/1)
    return x

def fft_manual2(x):
    y = np.zeros(x.shape)
    N = len(x)
    for k in range(len(x)):
        y[k] = (x * np.exp(-2* np.pi* np.sqrt(-1+0j)*k*np.arange(N)/N)).sum()
    return y

if __name__ == '__main__':
    x = np.array([1.0, 2.0, 1.0, -1.0, 1.5, 2,1,2])

    y = fft(x)
    y2 = fft_manual2(x)
    pprint(y)
    pprint(y2)
