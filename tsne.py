import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.manifold import TSNE
from argparse import ArgumentParser
from sklearn.preprocessing import LabelEncoder

# python -m tsne --data-dir embedding_model/dvectors/
# python -m tsne --data-dir datasets_preprocess/vctk/

def arg_parse():
    arg_p = ArgumentParser()
    arg_p.add_argument('--data-dir', required=True)
    return arg_p.parse_args()

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    num_class = len(labels)
    return labels, label_indices, num_class

def get_train_test(args):
    """
    Get available labels
    :param label:
    :param split_ratio:If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the test split.
    If int, represents the absolute number of test samples. If None, the value is set to the complement of the train size. By default, the value is set to 0.25. The default will change in version 0.21.
    It will remain 0.25 only if train_size is unspecified, otherwise it will complement the specified train_size.
    :param random_state: If int, random_state is the seed used by the random number generator; If RandomState instance,
    random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

    :return:
    """
    labels, indices, _ = get_labels("datasets/VCTK-Corpus/wav48")

    # Getting first arrays
    X = np.load(args.data_dir + labels[0] + '.npy')
    Y = np.load(args.data_dir + "classes-" + labels[0] + '.npy')

    # Append all of the dataset into one single array, same goes for y
    for i, label in enumerate(labels[1:]):
        x = np.load(args.data_dir + label + '.npy')
        y = np.load(args.data_dir + "classes-" + label + '.npy')
        X = np.vstack((X, x))
        Y = np.concatenate((Y, y))

    encoder = LabelEncoder()
    Y = encoder.fit_transform(Y)

    assert X.shape[0] == len(Y)

    return X, Y

def reshapeData(X, Y):
    """
    :param X_train:
    :param X_test:
    :param y_train:
    :param y_test:
    :return:
    """
    X = X.reshape(X.shape[0], 40* 188)

    return X, Y

if __name__=='__main__':
    args = arg_parse()

    X, Y = get_train_test(args)
    # X, Y = reshapeData(X,Y)

    X_embedded = TSNE(n_components=3, init='pca', verbose=2, random_state=0, perplexity=100).fit_transform(X)
    assert X.shape[0] == len(Y)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(X_embedded[:,0], X_embedded[:,1], X_embedded[:,2], c=Y, marker='.')

    plt.show()
    