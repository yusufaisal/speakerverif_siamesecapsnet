import numpy as np
import os
from os.path import exists
import librosa
import tensorflow as tf
import keras
from sklearn.model_selection import train_test_split
from keras.models import Model
from keras.layers import Dense, Conv2D, Input, Flatten
from capsulelayers import CapsuleLayer, PrimaryCap
from keras.utils import multi_gpu_model,to_categorical
from keras import callbacks

"""
@Scenarios
:param mode: bassicaly you can use 'one_ve_all' or 'augmented' audio
:param label: just VCTK speaker id. if it's in Train mode, this gonna used to load mfcc, save model, or-and load model
:param model_dir: contain model path. you should plain the text in the correct way
:param selected: parameter that focused on test data (only work if u in test mode). it would be 'actual_voices', 'cloned_voices', 'playback_recorded'    
"""
scenarios = {
    'mode'      : 'augmented',
    'label'     : 'idx0',
    'model_dir' : 'model_capsNet/',
    'selected'  : 'playback_recorded',
}

args = {
    "LOAD_MODEL": True,
    "TESTING"   : True,
    "DATA_PATH" : 'datasets/' + scenarios['label'] + "/",
    'DATA_SAVED': "datasets_preprocess/" + scenarios['label'] + "/",
    "MODEL_SAVED": "model_capsNet/" + scenarios['label'] + "/",
    "NUM_GPU"   : 1,

    "BATCH_SIZE": 16,
    "EPOCHS"    : 5,
    "ROUTINGS"  : 2,
    "num_class" : 2,
}
n_mels = 40

def wav2mfcc(wave):
    """
    :param wave:
    :return:
    """
    max_pad_len = 188
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=n_mels)
    pad_width = max_pad_len - mfcc.shape[1]
    mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def augPitch(samples, sample_rate):
    """
    :param samples:
    :param sample_rate:
    :return:
    """
    y_pitch = samples.copy()
    pitch_change = 8 * (np.random.uniform() - 0.5)
    y_pitch = librosa.effects.pitch_shift(y_pitch.astype('float64'),
                                          sample_rate, n_steps=pitch_change,
                                          bins_per_octave=24)
    return y_pitch

def buildModel(num_class):
    """
    :param num_class:
    :return:
    """
    x = Input(shape=(n_mels,188, 1))
    conv1 = Conv2D(filters=256, kernel_size=(1,1), strides=1, padding='valid', activation='relu', name='conv1')(x)
    primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=32, kernel_size=(1,1), strides=1, padding='valid')
    classcaps = CapsuleLayer(num_capsule=num_class, dim_capsule=16, routings=args['ROUTINGS'],
                             name='classcaps')(primarycaps)
    out_caps = Flatten(name='capsnet')(classcaps)
    fc7 = Dense(512, activation='relu', input_dim=16 * num_class)(out_caps)
    out = Dense(num_class, activation='softmax')(fc7)
    model = Model(inputs=x, outputs=out)

    model.compile(loss='binary_crossentropy',
                  optimizer=keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0),
                  metrics=['accuracy'])

    model.summary()
    return model

def get_labels(path=args['DATA_PATH']):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices, to_categorical(label_indices)

def get_train_test(label, split_ratio=0.8, random_state=None):
    """
    Get available labels
    :param label:
    :param split_ratio:If float, should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the test split.
    If int, represents the absolute number of test samples. If None, the value is set to the complement of the train size. By default, the value is set to 0.25. The default will change in version 0.21.
    It will remain 0.25 only if train_size is unspecified, otherwise it will complement the specified train_size.
    :param random_state: If int, random_state is the seed used by the random number generator; If RandomState instance,
    random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

    :return:
    """
    # labels, indices, _ = get_labels(args['DATA_PATH'])
    X = np.load(args['DATA_SAVED'] + label + '.npy')
    Y = np.load(args['DATA_SAVED'] + "classes-" + label + '.npy')
    num_class = 2
    print(X.shape[0] , len(Y))
    assert X.shape[0] == len(Y)

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=(1 - split_ratio), random_state=random_state, shuffle=True)
    return X_train, X_test, y_train, y_test, num_class

def reshapeData(X_train, X_test, y_train, y_test):
    """
    :param X_train:
    :param X_test:
    :param y_train:
    :param y_test:
    :return:
    """
    X_train = X_train.reshape(X_train.shape[0], n_mels, 188, 1)
    X_test = X_test.reshape(X_test.shape[0], n_mels, 188, 1)
    y_train_hot = to_categorical(y_train)
    y_test_hot = to_categorical(y_test)

    return X_train, X_test, y_train_hot, y_test_hot

def test(model, filepath):
    """
    :param model:
    :param filepath:
    :return:
    """
    print()
    print('-'*30+'TESTING'+'-'*30)
    for root, subdirs, files in os.walk(filepath):
        for file in files:
            # print(file)
            x_test = []
            wavpath = os.path.join(root, file)
            wave, sr = librosa.load(wavpath, mono=True, sr=None, offset=0, duration=2)
            mfcc_vector = wav2mfcc(wave)
            x_test.append(np.flip(mfcc_vector,axis=0))

            x_test = np.array(x_test)
            x_test = x_test.reshape(1, n_mels, 188, 1)

            y_pred = model.predict(x_test)
            print(' Verification result: ' + str(np.argmax(y_pred)) + "  " + str(y_pred) + " " + file)

def callback():
    """
    :return: [log, tb, checkpoint]
    """
    log = callbacks.CSVLogger(args['MODEL_SAVED'] + scenarios['label'] +'/log.csv')
    tb = callbacks.TensorBoard(log_dir=args['MODEL_SAVED'] + scenarios['label'] +'/tensorboard-logs',
                               batch_size=args['BATCH_SIZE'],histogram_freq=0,write_graph=True, write_images=True)
    filepath = args['MODEL_SAVED'] + scenarios['label'] + "weights-improvement2-{epoch:02d}-{val_acc:.2f}.hdf5"
    checkpoint = callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=0,
                                           save_best_only=True, mode='max')
    return [log, tb, checkpoint]

if __name__ == "__main__":
    """
    0. Initiation
    """
    wavpath = 'test_file/' + scenarios['label'] + '/'

    """
    1. Build Model
    The Capsule Network model. 
    """
    with tf.device('/gpu:0'):
        model = buildModel(args['num_class'])
        if args['LOAD_MODEL']:
            model.load_weights(scenarios['model_dir'] + scenarios['label'] + "/" + scenarios['label'] + '.hd5')

    """
    2. Checkpoint    
    Define callbacks_list for training, it's containing Log, TensorBoard, and Checkpoint
    I currently NOT using it, but if you want to try it you can add inside model.fit() method
    as 'callback=callback_list'
    """
    callbacks_list = callback()

    """
    3. Let's rockin' it!
    """
    if not args['TESTING']:
        X_train, X_test, y_train, y_test, num_class = get_train_test(label=scenarios['label'])
        X_train, X_test, y_train_hot, y_test_hot = reshapeData(X_train, X_test, y_train, y_test)


        if args['NUM_GPU'] > 1:
            # define muti-gpu model
            multi_model = multi_gpu_model(model, gpus=args['NUM_GPU'])
            multi_model.fit(X_train, y_train_hot, batch_size=args['BATCH_SIZE'], epochs=args['EPOCHS'], verbose=1,
                            validation_data=(X_test, y_test_hot))
        else:
            model.fit(X_train, y_train_hot, batch_size=args['BATCH_SIZE'], epochs=args['EPOCHS'], verbose=1,
                      validation_data=(X_test, y_test_hot),callbacks=callbacks_list)

        if not exists(args['MODEL_SAVED']):
            os.makedirs(args['MODEL_SAVED'])

        model.save(args['MODEL_SAVED'] + scenarios['label'] + '.hd5')
    else:
        test(model=model, filepath=args['DATA_PATH'] + "test/")

